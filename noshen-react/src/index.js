import React from 'react';
import ReactDOM from 'react-dom/client';
import App from './App';
import AppChakraProvider from './AppChakraProvider';

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <React.StrictMode>
    <AppChakraProvider>
      <App />
    </AppChakraProvider>
  </React.StrictMode>
);
