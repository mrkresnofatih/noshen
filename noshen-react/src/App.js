import React, { useEffect, useState } from "react";
import Header from "./Header";
import {
  useWindowHeight,
} from '@react-hook/window-size'
import {
  Flex,
  Tabs,
  TabList,
  TabPanels,
  Tab,
  TabPanel,
  Textarea,
  HStack,
  Button,
  Modal,
  ModalOverlay,
  ModalContent,
  ModalHeader,
  ModalFooter,
  ModalBody,
  ModalCloseButton,
  Text,
  useDisclosure,
  Input,
  VStack,
  Heading
} from '@chakra-ui/react';

function App() {
  const { isOpen, onOpen, onClose } = useDisclosure()
  const [oldTabTitle, setOldTabTitle] = useState("")
  const [newTabTitle, setNewTabTitle] = useState("")
  const onlyHeight = useWindowHeight()
  const defaultValueAppData = {
    "sample": {
      "title": "sample",
      "data": "a sample code"
    }
  };
  const [appData, setAppData] = useState(defaultValueAppData);

  const setTabContent = (tabTitle, data) => {
    setAppData(prev => {
      return {
        ...prev,
        [tabTitle]: {
          data: data,
          createdAt: prev[tabTitle].createdAt ?? Date.now()
        }
      }
    })
  }

  const createTab = () => {
    const newTabTitle = `NewTab_${String(Date.now())}`
    setAppData(prev => {
      return {
        ...prev,
        [newTabTitle]: {
          data: "",
          createdAt: Date.now()
        }
      }
    })
  }

  const closeTab = (tabTitle) => {
    setAppData(prev => {
      let newAppData = {}
      Object.keys(prev).filter(key => key !== tabTitle).forEach(key => {
        newAppData[key] = {
          ...prev[key]
        }
      })
      return newAppData;
    })
  }

  const initiateEditTabTitle = (selectedTabTitle) => {
    setOldTabTitle(selectedTabTitle);
    setNewTabTitle(selectedTabTitle);
    onOpen();
  }

  const initiateCloseEditTabTitle = () => {
    setOldTabTitle("");
    setNewTabTitle("");
    onClose();
  }

  const updateTabTitle = (tabTitle, newTabTitle) => {
    setAppData(prev => {
      let newAppData = {}
      newAppData[newTabTitle] = {
        ...prev[tabTitle]
      };
      Object.keys(prev).filter(key => key !== tabTitle).forEach(key => {
        newAppData[key] = {
          ...prev[key]
        }
      });
      return newAppData;
    });
    onClose();
  }

  const closeAllTabs = () => {
    setAppData(() => {
      return {}
    })
  }

  useEffect(() => {
    if (window.localStorage) {
      if (JSON.stringify(appData) !== JSON.stringify(defaultValueAppData)) {
        window.localStorage.setItem("noshen-app-data", JSON.stringify(appData));
      }
    }
    // eslint-disable-next-line
  }, [appData])

  useEffect(() => {
    if (!window.localStorage) {
      return;
    }
    if (!window.localStorage.getItem("noshen-app-data")) {
      return;
    }
    setAppData(JSON.parse(window.localStorage.getItem("noshen-app-data")));
  }, [])

  return (
    <React.Fragment>
      <Flex direction="column" h="100vh">
        <Header items={
          <HStack spacing="24px">
            <Button w="125px" size='sm' variant='solid' backgroundColor='#64CCC5' onClick={createTab}>New Tab</Button>
            <Button w="125px" size='sm' variant='solid' backgroundColor='#64CCC5' onClick={closeAllTabs}>Close All</Button>
          </HStack>
        } />
        <Tabs variant='enclosed' padding="24px">
          <TabList>
            {Object.keys(appData).sort((a, b) => { return appData[a].createdAt - appData[b].createdAt }).map((tabTitle, id) => (
              <Tab key={id}>
                <HStack spacing='12px'>
                  <Text fontSize='xs' onDoubleClick={() => initiateEditTabTitle(tabTitle)}>{tabTitle}</Text>
                  <Text fontSize='xs' color="#64CCC5" onClick={() => closeTab(tabTitle)}>X</Text>
                </HStack>
              </Tab>
            ))}
          </TabList>
          <TabPanels>
            {Object.keys(appData).sort((a, b) => { return appData[a].createdAt - appData[b].createdAt }).map((tabTitle, id) => (
              <TabPanel key={id}>
                <Textarea
                  value={appData[tabTitle].data}
                  onChange={(e) => setTabContent(tabTitle, e.target.value)}
                  placeholder='Write your notes here'
                  size='sm'
                  h={onlyHeight - 200}
                />
              </TabPanel>
            ))}
          </TabPanels>
        </Tabs>
      </Flex>
      <Modal isOpen={isOpen} onClose={onClose}>
        <ModalOverlay />
        <ModalContent>
          <ModalHeader>
            <Heading as='h3' size='lg'>Update Tab Title</Heading>
          </ModalHeader>
          <ModalCloseButton />
          <ModalBody>
            <VStack spacing="12px">
              <Heading as='h4' size='md'>{"Changing Tab Title from:"}</Heading>
              <Input isDisabled={true} size='md' value={oldTabTitle} onChange={e => setNewTabTitle(e.target.value)} />
              <Heading as='h4' size='md'>{"To:"}</Heading>
              <Input size='md' value={newTabTitle} onChange={e => setNewTabTitle(e.target.value)} />
            </VStack>
          </ModalBody>

          <ModalFooter>
            <Button backgroundColor='#176B87' color="white" mr={3} onClick={() => updateTabTitle(oldTabTitle, newTabTitle)}>
              Save
            </Button>
            <Button backgroundColor='#64CCC5' color="white" mr={3} onClick={() => initiateCloseEditTabTitle()}>
              Close
            </Button>
          </ModalFooter>
        </ModalContent>
      </Modal>
    </React.Fragment>
  );
}

export default App;
