import React from 'react'
import { ChakraProvider, extendTheme } from '@chakra-ui/react'

const theme = extendTheme({
    fonts: {
        heading: `'JetBrains Mono', monospace`,
        body: `'JetBrains Mono', monospace`,
    },
})


const AppChakraProvider = ({children}) => {
    return (
        <ChakraProvider theme={theme}>{children}</ChakraProvider>
    )
}

export default AppChakraProvider