import { Flex, Heading } from '@chakra-ui/react'
import React from 'react'

const Header = ({items}) => {
    return (
        <Flex justifyContent="space-between" backgroundColor="black" padding="12px 24px">
            <Heading as='h1' size='xl' noOfLines={1} color="white">
                Noshen
            </Heading>
            {items}
        </Flex>
    )
}

export default Header